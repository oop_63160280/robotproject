/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.robotproject2;

/**
 *
 * @author User
 */
public class TableMap {

    private int width;
    private int hight;
    private Robot robot;
    private Bomb bomb;

    public TableMap(int width, int hight) {
        this.width = width;
        this.hight = hight;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
    }

    public void showMap() {
        showTitle();
        for (int y = 0; y < hight; y++) {
            for (int x = 0; x < width; x++) {
                if(robot.isOn(x, y)){
                    showRobot();
                }else if(bomb.isOn(x, y)){
                    showBomb();
                }else{
                    showCell();
                }
            }
            showNewLine();
        }

    }

    private void showTitle() {
        System.out.println("Map");
    }

    private void showNewLine() {
        System.out.println("");
    }

    private void showCell() {
        System.out.print("-");
    }

    private void showBomb() {
        System.out.print(bomb.getsymbol());
    }

    private void showRobot() {
        System.out.print(robot.getSymbol());
    }

    public boolean inMap(int x, int y) {
        // x=> 0-width-1 ,y=> 0-hight-1
        // (x >=0 && x < width) && (y >= 0&& y < hight)
        return (x >= 0 && x <= (width - 1)) && (y >= 0 && y <= (hight - 1));
    }

    public boolean isBomb(int x, int y) {
        return bomb.isOn(x, y);
    }
}
